package DAOEXERCISE.Cinema.Interface;

import DAOEXERCISE.Cinema.beans.Acteur;
import com.mongodb.MongoException;
import java.sql.SQLException;
import java.util.LinkedList;

public interface IActeurDao {

    void create(Acteur obj) throws SQLException, MongoException;
    void updateFromId(Acteur obj) throws SQLException, MongoException;
    void delete(Acteur obj) throws SQLException, MongoException;
    void deleteById(Integer id) throws SQLException, MongoException;
    void deleteAll() throws SQLException, MongoException;
    Acteur findById(Integer id) throws SQLException, MongoException;
    Integer findIdByFullName(String lastname, String firstname) throws SQLException, MongoException;
    Acteur[] findAll() throws SQLException, MongoException;
    LinkedList<Integer> findRelatedMovie(Integer id) throws SQLException, MongoException;
}
