package DAOEXERCISE.Cinema.Interface;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class DAO<T, K> {

    public abstract void create(T obj) throws SQLException,MongoException;

    public abstract void updateFromId(T obj) throws SQLException,MongoException;

    public abstract void delete(T obj) throws SQLException,MongoException ;

    public abstract void deleteById(K id) throws SQLException,MongoException;

    public abstract T findById(K id) throws SQLException,MongoException;

    public abstract T[] findAll() throws SQLException,MongoException;
}
