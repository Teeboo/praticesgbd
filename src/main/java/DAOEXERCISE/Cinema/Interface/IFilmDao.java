package DAOEXERCISE.Cinema.Interface;

import DAOEXERCISE.Cinema.beans.Film;
import com.mongodb.MongoException;

import java.sql.SQLException;
import java.util.LinkedList;


public interface IFilmDao {

    void create(Film obj) throws SQLException, MongoException;
    void linkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException, MongoException;
    void unLinkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException, MongoException;
    void updateFromId(Film obj) throws SQLException, MongoException;
    void delete(Film obj) throws SQLException, MongoException;
    void deleteById(Integer id) throws SQLException, MongoException;
    void deleteAll()throws SQLException, MongoException;
    Film findById(Integer id) throws SQLException, MongoException;
    Film findByNameAndYear(String name, int year) throws SQLException, MongoException;
    Film[] findAll() throws SQLException, MongoException;
    LinkedList<Integer> findRelatedActor(Integer idFilm) throws SQLException, MongoException;
}
