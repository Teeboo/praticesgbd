package DAOEXERCISE.Cinema.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    private String databaseType;
    private Properties properties = new Properties();

    public ConfigReader() {

        try (InputStream input =  new FileInputStream("src/main/java/DAOEXERCISE/Cinema/config/config.properties")){

            if (input == null) {
                throw new IOException("Property file not found in the classpath");
            }

            properties.load(input);
            databaseType = properties.getProperty("database.type");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getDatabaseType() {
        return databaseType;
    }

}