package DAOEXERCISE.Cinema.beans;

import java.util.LinkedList;

public class Acteur {

    // VARIABLES

    private String lastname;
    private String firstname;
    private Integer idActeur;


    // CONSTRUCTOR

    public Acteur(){
    }

    public Acteur(String lastname, String firstname) {
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public Acteur(String lastname, String firstname, Integer idActeur) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.idActeur = idActeur;
    }



    // GETTER SETTER

    public String getLastname(){
        return lastname;
    }

    public void setLastname(String lastname){
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Integer getIdActeur() {
        return idActeur;
    }

    public void setIdActeur(Integer idActeur) {
        this.idActeur = idActeur;
    }


    // FUNCTION


    @Override
    public String toString() {
        String acteur;
        acteur = " ID : "+idActeur+ " - "+ this.lastname + " "+ this.firstname;
        return acteur;
    }
}
