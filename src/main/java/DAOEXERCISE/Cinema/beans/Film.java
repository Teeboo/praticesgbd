package DAOEXERCISE.Cinema.beans;

import java.util.Collections;
import java.util.LinkedList;

public class Film {

    // VARIABLES

    private String name;
    private int year;
    private Integer idFilm;
    private LinkedList<Integer> idActeur;


    // CONSTRUCTOR

    public Film(){
    }

    public Film(String name, int year) {
        this.name = name;
        this.year = year;
    }
    public Film(String name, int year, Integer idFilm) {
        this.name = name;
        this.year = year;
        this.idFilm = idFilm;
    }

    public Film(String name, int year, LinkedList<Integer> idActeur) {
        this.name = name;
        this.year = year;
        this.idActeur = idActeur;
    }

    public Film(String name, int year, Integer idFilm, LinkedList<Integer> idActeur) {

        Collections.sort(idActeur);
        this.name = name;
        this.year = year;
        this.idFilm = idFilm;
        this.idActeur = idActeur;
    }


    // GETTER SETTER


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Integer getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(Integer idFilm) {
        this.idFilm = idFilm;
    }

    public LinkedList<Integer> getIdActeur() {
        return idActeur;
    }
    public void setIdActeur(LinkedList<Integer> idActeur) {
        this.idActeur = idActeur;
    }

    // FUNCTION


    @Override
    public String toString() {
        String film = " ID : "+idFilm+ " "+ this.name + " - "+ this.year;

        if(idActeur != null){
            film += "\n Acteur : "+idActeur.toString();
        }

        return film;
    }
}
