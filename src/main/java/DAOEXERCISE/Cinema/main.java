package DAOEXERCISE.Cinema;


import DAOEXERCISE.Cinema.Dao.ActeurDAO;
import DAOEXERCISE.Cinema.Dao.Factory.DAOFactory;
import DAOEXERCISE.Cinema.Dao.FilmDAO;
import DAOEXERCISE.Cinema.beans.Acteur;
import DAOEXERCISE.Cinema.beans.Film;

import java.sql.SQLException;
import java.util.LinkedList;


public class main {


    public static void main(String[] args) throws SQLException{


        DAOFactory daoFactory = DAOFactory.connectTo();
        ActeurDAO acteurdao = daoFactory.getActeurDAO();
        FilmDAO filmdao = daoFactory.getFilmDAO();

        filmdao.deleteAll();
        acteurdao.deleteAll();

        System.out.println("\n============ CREATION ACTOR ============\n");

        acteurdao.create(new Acteur("Suchet","David",1));
        acteurdao.create(new Acteur("Reno","Jean",2));
        acteurdao.create(new Acteur("Defunes","Louis",3));
        acteurdao.create(new Acteur("Hanks","Tom",4));
        acteurdao.create(new Acteur("Jugnot","Gerard"));
        Acteur acteur1 = new Acteur("Leroy", "Jenkins");
        acteurdao.create(acteur1);
        PrintActeur(acteurdao);

        System.out.println("\n============ UPDATING : LEROY JENKINS -> LEROY DOMINIQUE ============\n");

        acteurdao.updateFromId(new Acteur("Leroy","Dominique",acteurdao.findIdByFullName("Leroy","Jenkins")));

        PrintActeur(acteurdao);

        System.out.println("\n============ DELETING : LEROY DOMINIQUE ============\n");

        acteurdao.delete(new Acteur("Leroy","Dominique",acteurdao.findIdByFullName("Leroy","Dominique")));
        PrintActeur(acteurdao);

        System.out.println("\n============ FIND ACTOR : ID 1 ============\n");

        System.out.println(acteurdao.findById(1));

        System.out.println("\n============ CREATE MOVIE ============\n");

        filmdao.create(new Film("La grande vadrouille",1996,1));
        filmdao.create(new Film("Les Aventures de Rabbi Jacob",1973,2));
        filmdao.create(new Film("Le prenom",2012,3));
        filmdao.create(new Film("Le Bon, la Brute et le Truand",1972,4));
        filmdao.create(new Film("Le Gendarme",1964));
        filmdao.create(new Film("Le Diner de cons",1998));
        filmdao.create(new Film("Cyrano de Bergerac",1990));
        PrintFilm(filmdao);

        System.out.println("\n============ CREATION MOVIE : Les Tontons flingueurs ============\n");

        LinkedList<Integer> actorsId = new LinkedList<>();
        actorsId.add(1);
        actorsId.add(2);

        filmdao.create(new Film("Les Tontons flingueurs",1963,actorsId));
        PrintFilm(filmdao);

        System.out.println("\n============ UPDATE MOVIE : Les Tontons flingueurs => 1970 ============\n");

        filmdao.updateFromId(new Film("Les Tontons flingueurs 2",1970,filmdao.findByNameAndYear("Les Tontons flingueurs",1963).getIdFilm()));
        PrintFilm(filmdao);

        System.out.println("\n============ ADD ACTOR TO Les Tontons flingueurs => 1970 ============\n");

        filmdao.linkFilmToActor(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970).getIdFilm(),3);
        filmdao.linkFilmToActor(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970).getIdFilm(),4);

        System.out.println("Acteur du film 'Les tontons flingeurs' = ");

        LinkedList<Integer> actorsId2 = filmdao.findRelatedActor(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970).getIdFilm());


        System.out.println(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970));

        for(Integer element : actorsId2){
            System.out.println(acteurdao.findById(element));
        }

        System.out.println("\n============ SHOW MOVIE FROM ACTOR ID ============\n");

        System.out.println("Film de l'acteur 'David Suchet' :");
        LinkedList<Integer> relatedMovieList = acteurdao.findRelatedMovie(1);

        for(Integer element : relatedMovieList){
            System.out.println(filmdao.findById(element));
        }

        System.out.println("\n============ REMOVE ACTOR TO Les Tontons flingueurs => 1970 ============\n");

        filmdao.unLinkFilmToActor(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970).getIdFilm(),3);

        System.out.println("Acteur du film 'Les tontons flingeurs' : ");

        actorsId2 = filmdao.findRelatedActor(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970).getIdFilm());

        for(Integer element : actorsId2){
            System.out.println(acteurdao.findById(element));
        }


        System.out.println("\n============ DELETING MOVIE : Les Tontons flingueurs 2 ============\n");

        filmdao.delete(filmdao.findByNameAndYear("Les Tontons flingueurs 2",1970));
        PrintFilm(filmdao);

        System.out.println("\n============ DELETE EVERYTHING ============\n");

        filmdao.deleteAll();
        acteurdao.deleteAll();

        PrintFilm(filmdao);
        PrintActeur(acteurdao);

    }


    public static void PrintActeur(ActeurDAO acteurDAO) throws SQLException {

        Acteur[] acteur = acteurDAO.findAll();

        if(acteur != null) {

            for(Acteur element : acteur) {
                System.out.println(element);
            }
        }
    }


    public static void PrintFilm(FilmDAO filmDAO) throws SQLException {

        Film[] film = filmDAO.findAll();

        if(film != null) {

            for(Film element : film) {
                System.out.println(element);
            }
        }
    }

}
