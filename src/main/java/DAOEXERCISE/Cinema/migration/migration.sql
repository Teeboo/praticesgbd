CREATE DATABASE cinema;
USE cinema;

CREATE TABLE film (
    idFilm INT unsigned PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    year INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE acteur (
    idActeur  INT unsigned PRIMARY KEY AUTO_INCREMENT,
    lastname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE film_acteur (
    idActeur  INT unsigned,
    idFilm INT unsigned,
    CONSTRAINT FK_film FOREIGN KEY (idFilm) REFERENCES film(idFilm),
    CONSTRAINT FK_acteur FOREIGN KEY (idActeur) REFERENCES acteur(idActeur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;