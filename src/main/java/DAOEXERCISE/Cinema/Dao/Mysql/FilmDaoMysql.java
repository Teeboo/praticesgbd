package DAOEXERCISE.Cinema.Dao.Mysql;

import DAOEXERCISE.Cinema.Interface.IFilmDao;
import DAOEXERCISE.Cinema.beans.Film;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class FilmDaoMysql implements IFilmDao {


    // VARIABLES

    private Connection connection;
    public final String tableName = "film";
    public final String keyName = "idFilm";
    private final String transitiontableName = "film_acteur";


    // CONSTRUCTOR


    public FilmDaoMysql(Connection connection) {
        this.connection = connection;

    }


    // FUNCTION


    public void create(Film obj) throws SQLException {

        String rq;
        PreparedStatement ps;

        if(obj.getIdFilm() == null){
            rq = String.format("INSERT INTO %s (name, year) VALUES (?,?);", tableName);
            ps = connection.prepareStatement(rq, Statement.RETURN_GENERATED_KEYS);
        } else {
            rq = String.format("INSERT INTO %s (name, year,%s) VALUES (?,?,?);", tableName,keyName);
            ps = connection.prepareStatement(rq);
        }

        ps.setObject(1, obj.getName());
        ps.setObject(2, obj.getYear());

        if(obj.getIdFilm() != null){
            ps.setObject(3, obj.getIdFilm());
        }
        ps.executeUpdate();

        if (obj.getIdFilm() == null){

            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                obj.setIdFilm(rs.getInt(1));
            }
            rs.close();
        }

        if(obj.getIdActeur() != null){
            for(Integer element : obj.getIdActeur()){

                String rq2 = String.format("INSERT INTO %s (%s, idActeur) VALUES (?,?);", transitiontableName,keyName);
                PreparedStatement ps2 = connection.prepareStatement(rq2);
                ps2.setObject(1, obj.getIdFilm());
                ps2.setObject(2, element);
                ps2.executeUpdate();
            }
        }
    }


    public void updateFromId(Film obj)throws SQLException{

        String rq = String.format("UPDATE %s SET name = ?, year = ? WHERE %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, obj.getName());
        ps.setObject(2, obj.getYear());
        ps.setObject(3,obj.getIdFilm());
        ps.executeUpdate();
    }


    public void delete(Film obj) throws SQLException{

        String rq2 = String.format("DELETE FROM %s WHERE %s = ?;", transitiontableName,keyName);
        PreparedStatement ps2 = connection.prepareStatement(rq2);
        ps2.setObject(1, obj.getIdFilm());
        ps2.executeUpdate();

        String rq = String.format("DELETE FROM %s WHERE %s = ? AND %s = ? AND %s = ?;", tableName,keyName,"name","year");
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, obj.getIdFilm());
        ps.setObject(2, obj.getName());
        ps.setObject(3, obj.getYear());
        ps.executeUpdate();

    }


    public void deleteById(Integer id) throws SQLException{


        String rq2 = String.format("DELETE FROM %s WHERE %s = ?;", transitiontableName,keyName);
        PreparedStatement ps2 = connection.prepareStatement(rq2);
        ps2.setObject(1, id);
        ps2.executeUpdate();

        String rq = String.format("DELETE FROM %s WHERE %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, id);
        ps.executeUpdate();

    }

    public void deleteAll() throws SQLException{

        String rq2 = String.format("DELETE FROM %s;", transitiontableName);
        PreparedStatement ps2 = connection.prepareStatement(rq2);
        ps2.executeUpdate();

        String rq = String.format("DELETE FROM %s;", tableName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.executeUpdate();

    }

    public Film findById(Integer id) throws SQLException{

        String rq = String.format("SELECT * FROM %s WHERE %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            Film film = rsLineToFilm(rs);

            String rq2 = String.format("SELECT idActeur FROM %s WHERE %s = ?;", transitiontableName,keyName);
            PreparedStatement ps2 = connection.prepareStatement(rq2);
            ps2.setObject(1, film.getIdFilm());
            ResultSet rs2 = ps2.executeQuery();
            LinkedList<Integer> acteurIdList = new LinkedList<>();

            while(rs2.next()){
                acteurIdList.add(rs2.getInt("idActeur"));
            }
            if(!acteurIdList.isEmpty()){
                film.setIdActeur(acteurIdList);
            }
            return film;
        }
        return null;
    }

    public Film findByNameAndYear(String name,int year) throws SQLException {

        String rq = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?;", tableName,"name","year");
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, name);
        ps.setObject(2, year);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            Film film = rsLineToFilm(rs);

            String rq2 = String.format("SELECT idActeur FROM %s WHERE %s = ?;", transitiontableName,keyName);
            PreparedStatement ps2 = connection.prepareStatement(rq2);
            ps2.setObject(1, film.getIdFilm());
            ResultSet rs2 = ps2.executeQuery();
            LinkedList<Integer> acteurIdList = new LinkedList<>();

            while(rs2.next()){
                acteurIdList.add(rs2.getInt("idActeur"));
            }
            if(!acteurIdList.isEmpty()){
                film.setIdActeur(acteurIdList);
            }
            return film;
        }
        return null;
    }

    public Film[] findAll()throws SQLException{

        String rq = String.format("SELECT f.idFilm,f.name,f.year,GROUP_CONCAT(fa.idActeur) as acteur FROM %s as f LEFT JOIN film_acteur as fa on f.idFilm = fa.idFilm GROUP BY f.idFilm, f.name, f.year;", tableName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ResultSet rs = ps.executeQuery();

        List<Film> filmlist = new LinkedList<>();

        while (rs.next()) {

            Film film = rsLineToFilm(rs);
            LinkedList<Integer> idActeurList = new LinkedList<>();
            String idActeur = rs.getString("acteur");

            if (idActeur != null && !idActeur.isEmpty()) {
                for (String id : idActeur.split(",")) {
                    idActeurList.add(Integer.parseInt(id));
                }
                film.setIdActeur(idActeurList);
            }
            filmlist.add(film);
        }

        return filmlist.toArray(Film[]::new);
    }

    public LinkedList<Integer> findRelatedActor(Integer idFilm) throws SQLException {

        LinkedList<Integer> ActorIdList = new LinkedList<>();
        String rq = String.format("SELECT * FROM %s WHERE %s = ?", transitiontableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, idFilm);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            ActorIdList.add(rs.getInt("idActeur"));
        }
        return ActorIdList;
    }

    public void linkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException{

        String rq = String.format("INSERT INTO %s (%s, idActeur) VALUES (?,?);", transitiontableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, idFilm);
        ps.setObject(2, idActeur);
        ps.executeUpdate();

    }
    public void unLinkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException{

        String rq = String.format("DELETE FROM %s WHERE %s = ? AND idActeur = ? ;", transitiontableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, idFilm);
        ps.setObject(2, idActeur);
        ps.executeUpdate();
    }

    public Film rsLineToFilm(ResultSet rs) throws SQLException {

        if (rs.getRow() == 0){
            return null;
        }

        Film film = new Film(rs.getString("name"), rs.getInt("year"),rs.getInt("idFilm"));
        return film;
    }
}
