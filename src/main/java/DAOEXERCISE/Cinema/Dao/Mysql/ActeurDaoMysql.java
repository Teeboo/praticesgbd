package DAOEXERCISE.Cinema.Dao.Mysql;

import DAOEXERCISE.Cinema.Interface.IActeurDao;
import DAOEXERCISE.Cinema.beans.Acteur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ActeurDaoMysql implements IActeurDao {


    // VARIABLES

    private Connection connection;
    public final String tableName = "acteur";
    public final String keyName = "idActeur";
    private final String transitiontableName = "film_acteur";

    // CONSTRUCTOR


    public ActeurDaoMysql(Connection connection) {
        this.connection = connection;
    }

    // FUNCTION


    public void create(Acteur obj) throws SQLException {

        if(obj.getIdActeur() == null ){

            String rq = String.format("INSERT INTO %s (lastname, firstname) VALUES (?,?);", tableName);
            PreparedStatement ps = connection.prepareStatement(rq);
            ps.setObject(1, obj.getLastname());
            ps.setObject(2, obj.getFirstname());
            ps.executeUpdate();
        }else {

            String rq = String.format("INSERT INTO %s (lastname, firstname,%s) VALUES (?,?,?);", tableName,keyName);
            PreparedStatement ps = connection.prepareStatement(rq);
            ps.setObject(1, obj.getLastname());
            ps.setObject(2, obj.getFirstname());
            ps.setObject(3,obj.getIdActeur());
            ps.executeUpdate();
        }
    }


    public void updateFromId(Acteur obj)throws SQLException{

        String rq = String.format("UPDATE %s SET lastname = ?, firstname = ? WHERE %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, obj.getLastname());
        ps.setObject(2, obj.getFirstname());
        ps.setObject(3,obj.getIdActeur());
        ps.executeUpdate();
    }


    public void delete(Acteur obj)throws SQLException{

        String rq = String.format("DELETE FROM %s where %s = ? AND %s = ? AND %s = ? ;", tableName,keyName,"lastname","firstname");
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, obj.getIdActeur());
        ps.setObject(2, obj.getLastname());
        ps.setObject(3, obj.getFirstname());
        ps.executeUpdate();
    }


    public void deleteById(Integer id) throws SQLException{

        String rq = String.format("DELETE FROM %s where %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, id);
        ps.executeUpdate();
    }

    public void deleteAll() throws SQLException {

        String rq = String.format("DELETE FROM %s;", tableName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.executeUpdate();
    }

    public Acteur findById(Integer id)throws SQLException{

        String rq = String.format("SELECT * FROM %s where %s = ?;", tableName,keyName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, id);
        ResultSet rs = ps.executeQuery();

        rs.next();
        return rsLineToObj(rs);
    }

    public Integer findIdByFullName(String lastname, String firstname )throws SQLException{

        String rq = String.format("SELECT * FROM %s where %s = ? AND %s = ?;", tableName,"lastname","firstname");
        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, lastname);
        ps.setObject(2, firstname);
        ResultSet rs = ps.executeQuery();

        rs.next();
        return rsLineToObj(rs).getIdActeur();
    }

    public Acteur[] findAll() throws SQLException {

        String rq = String.format("SELECT * FROM %s;", tableName);
        PreparedStatement ps = connection.prepareStatement(rq);
        ResultSet rs = ps.executeQuery();

        List<Acteur> Acteurlist = new LinkedList<>();
        while (rs.next()) {
            Acteurlist.add(rsLineToObj(rs));
        }

        return Acteurlist.toArray(Acteur[]::new);
    }

    public LinkedList<Integer> findRelatedMovie(Integer id) throws SQLException {

        String rq = String.format("SELECT idFilm FROM %s INNER JOIN %s a on %s.idActeur = a.idActeur WHERE a.idActeur = ?;", transitiontableName,tableName,transitiontableName);

        PreparedStatement ps = connection.prepareStatement(rq);
        ps.setObject(1, id);
        ResultSet rs = ps.executeQuery();

        LinkedList<Integer> FilmIdlist = new LinkedList<>();
        while (rs.next()) {
            FilmIdlist.add(rs.getInt("idFilm"));
        }

        return FilmIdlist;
    }

    public Acteur rsLineToObj(ResultSet rs) throws SQLException {

        if (rs.getRow() == 0){
            return null;
        }
        Acteur acteur = new Acteur(rs.getString("lastname"), rs.getString("firstname"),rs.getInt("idActeur"));
        return acteur;
    }
}
