package DAOEXERCISE.Cinema.Dao;

import DAOEXERCISE.Cinema.Dao.Factory.FilmDaoFactory;
import DAOEXERCISE.Cinema.Interface.DAO;
import DAOEXERCISE.Cinema.Interface.IFilmDao;
import DAOEXERCISE.Cinema.beans.Film;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

public class FilmDAO extends DAO<Film,Integer> {


    // VARIABLES

    private IFilmDao filmDao;


    // CONSTRUCTOR

    public FilmDAO(MongoClient connection) {
        filmDao = FilmDaoFactory.getDao(connection);
    }

    public FilmDAO(Connection connection) {
        filmDao = FilmDaoFactory.getDao(connection);
    }


    // FUNCTION


    @Override
    public void create(Film obj)throws SQLException,MongoException {

        filmDao.create(obj);
    }

    @Override
    public void updateFromId(Film obj) throws SQLException,MongoException {

        filmDao.updateFromId(obj);
    }

    @Override
    public void delete(Film obj) throws SQLException,MongoException {

        filmDao.delete(obj);
    }

    @Override
    public void deleteById(Integer id) throws SQLException,MongoException {

        filmDao.deleteById(id);
    }

    public void deleteAll() throws SQLException,MongoException {

        filmDao.deleteAll();
    }


    @Override
    public Film findById(Integer id)throws SQLException,MongoException {

        return filmDao.findById(id);
    }

    public Film findByNameAndYear(String name, int year)throws SQLException,MongoException  {

        return filmDao.findByNameAndYear(name, year);

    }


    @Override
    public Film[] findAll() throws SQLException,MongoException {

        return filmDao.findAll();
    }

    public LinkedList<Integer> findRelatedActor(Integer idFilm) throws SQLException,MongoException {

        return filmDao.findRelatedActor(idFilm);
    }

    public void linkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException, MongoException{

        filmDao.linkFilmToActor(idFilm,idActeur);
    }

    public void unLinkFilmToActor(Integer idFilm, Integer idActeur) throws SQLException, MongoException{

        filmDao.unLinkFilmToActor(idFilm,idActeur);
    }




}
