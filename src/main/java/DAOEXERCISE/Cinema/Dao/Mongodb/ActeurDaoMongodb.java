package DAOEXERCISE.Cinema.Dao.Mongodb;

import DAOEXERCISE.Cinema.Interface.IActeurDao;
import DAOEXERCISE.Cinema.beans.Acteur;
import DAOEXERCISE.Cinema.beans.Film;
import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;
import java.sql.SQLException;
import java.util.*;

public class ActeurDaoMongodb implements IActeurDao {


    // VARIABLES

    private MongoDatabase database;
    private final String DatabaseName = "cinema";
    public final String tableName = "acteur";
    public final String keyName = "idActeur";


    // CONSTRUCTOR


    public ActeurDaoMongodb(MongoClient connection) {
        this.database = connection.getDatabase(DatabaseName);
    }

    // FUNCTION


    public void create(Acteur obj){

        if(obj.getIdActeur()!=null){

            Document document = new Document()
                    .append(keyName,obj.getIdActeur())
                    .append("lastname",obj.getLastname())
                    .append("firstname",obj.getFirstname());
            database.getCollection(tableName).insertOne(document);

        } else {

            Document documentHighestIdfilm = database.getCollection(tableName).find().sort(Sorts.descending(keyName)).first();
            Integer lastId;

            if (documentHighestIdfilm != null) {
                 lastId = documentHighestIdfilm.getInteger(keyName)+1;
            } else {
                lastId = 1;
            }

            Document document = new Document()
                    .append("lastname",obj.getLastname())
                    .append("firstname",obj.getFirstname())
                    .append(keyName,lastId);
            database.getCollection(tableName).insertOne(document);
        }

    }


    public void updateFromId(Acteur obj){

        Bson filter = Filters.eq(keyName,obj.getIdActeur());
        Bson update = Updates.combine(Updates.set("lastname",obj.getLastname()),Updates.set("firstname",obj.getFirstname()));
        database.getCollection(tableName).updateOne(filter,update);
    }


    public void delete(Acteur obj){

        if(obj.getIdActeur()!=null){
            Bson filter = Filters.and(
                    Filters.eq(keyName, obj.getIdActeur()),
                    Filters.eq("lastname", obj.getLastname()),
                    Filters.eq("firstname", obj.getFirstname())
            );
            database.getCollection(tableName).deleteOne(filter);
        } else {
            Bson filter = Filters.and(
                    Filters.eq("lastname", obj.getLastname()),
                    Filters.eq("firstname", obj.getFirstname())
            );
            database.getCollection(tableName).deleteMany(filter);
        }
    }


    public void deleteById(Integer id){

        Bson filter = Filters.eq(keyName, id);
        database.getCollection(tableName).deleteOne(filter);
    }

    public void deleteAll(){
        database.getCollection(tableName).deleteMany(new Document());
    }


    public Acteur findById(Integer id){

        Bson filter = Filters.eq(keyName, id);
        FindIterable<Document> iterable = database.getCollection(tableName).find(filter);

        if(iterable.iterator().hasNext()){
            Document document =  iterable.iterator().next();
            return new Gson().fromJson(document.toJson(), Acteur.class);
        } else{
            return null;
        }
    }

    public Integer findIdByFullName(String lastname, String firstname ){

        Bson filter = Filters.and(
                Filters.eq("lastname",lastname),
                Filters.eq("firstname",firstname)
        );

        FindIterable<Document> iterable = database.getCollection(tableName).find(filter);

        if(iterable.iterator().hasNext()){
            Document document =  iterable.iterator().next();
            return document.getInteger(keyName);
        } else{
            return null;
        }
    }

    public Acteur[] findAll(){

        Gson gson = new Gson();
        List<Acteur> acteurlist = new LinkedList<>();
        FindIterable<Document> iterable = database.getCollection(tableName).find();
        Iterator<Document> iterator = iterable.iterator();

        if(iterator.hasNext()){
            while (iterator.hasNext()) {
                Document document = iterator.next();
                Acteur acteur = gson.fromJson(document.toJson(), Acteur.class);
                acteurlist.add(acteur);
            }
            return  acteurlist.toArray(Acteur[]::new);
        } else {
            return null;
        }
    }

    public LinkedList<Integer> findRelatedMovie(Integer id) throws SQLException {

        LinkedList<Integer> RelatedMoviesId = new LinkedList<>();

        Gson gson = new Gson();
        List<Film> filmlist = new LinkedList<>();
        FindIterable<Document> iterable = database.getCollection("film").find();
        Iterator<Document> iterator = iterable.iterator();

        if(iterator.hasNext()){
            while (iterator.hasNext()) {
                Document document = iterator.next();
                Film film = gson.fromJson(document.toJson(), Film.class);
                filmlist.add(film);
            }
        }

        for(Film film : filmlist){
            if(film.getIdActeur() != null && film.getIdActeur().contains(id)){
                RelatedMoviesId.add(film.getIdFilm());
            }
        }
        return RelatedMoviesId;
    }


}
