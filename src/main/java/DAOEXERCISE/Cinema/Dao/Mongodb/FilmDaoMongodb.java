package DAOEXERCISE.Cinema.Dao.Mongodb;

import DAOEXERCISE.Cinema.Interface.IFilmDao;
import DAOEXERCISE.Cinema.beans.Film;
import com.google.gson.Gson;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class FilmDaoMongodb implements IFilmDao {

    // VARIABLES

    private MongoDatabase database;
    private final String DatabaseName = "cinema";
    public final String tableName = "film";
    public final String keyName = "idFilm";
    public final String LinkedActeur = "idActeur";


    // CONSTRUCTOR

    public FilmDaoMongodb(MongoClient connection) {
        this.database = connection.getDatabase(DatabaseName);
    }

    // FUNCTION


    public void create(Film obj) throws MongoException {


        if(obj.getIdFilm()!=null){

            Document document = new Document()
                    .append("name",obj.getName())
                    .append("year",obj.getYear())
                    .append(keyName,obj.getIdFilm())
                    .append(LinkedActeur,obj.getIdActeur());
            database.getCollection(tableName).insertOne(document);

        } else {

            Document documentHighestIdfilm = database.getCollection(tableName).find().sort(Sorts.descending(keyName)).first();
            Integer lastId;

            if (documentHighestIdfilm != null) {
                lastId = documentHighestIdfilm.getInteger(keyName)+1;
            } else {
                lastId = 1;
            }

            Document document = new Document()
                    .append("name",obj.getName())
                    .append("year",obj.getYear())
                    .append(keyName,lastId)
                    .append(LinkedActeur,obj.getIdActeur());
            database.getCollection(tableName).insertOne(document);
        }



    }

    public void linkFilmToActor(Integer idFilm, Integer idActeur) throws MongoException{

        Film film = this.findById(idFilm);
        LinkedList<Integer> ActorFromFilm= null;

        if(film != null){

            ActorFromFilm = film.getIdActeur();
            ActorFromFilm.add(idActeur);
            Collections.sort(ActorFromFilm);

            Bson filter = Filters.eq(keyName,idFilm);
            Bson update = Updates.set("idActeur",ActorFromFilm);
            database.getCollection(tableName).updateOne(filter,update);
        }
    }

    public void unLinkFilmToActor(Integer idFilm, Integer idActeur) throws MongoException{

        Film film = this.findById(idFilm);
        LinkedList<Integer> ActorFromFilm= null;

        if(film != null){

            ActorFromFilm = film.getIdActeur();
            ActorFromFilm.remove(idActeur);
            Collections.sort(ActorFromFilm);

            Bson filter = Filters.eq(keyName,idFilm);
            Bson update = Updates.set("idActeur",ActorFromFilm);
            database.getCollection(tableName).updateOne(filter,update);
        }
    }


    public void updateFromId(Film obj) throws MongoException {

        if(obj == null || obj.getIdFilm() == null){
            return;
        }

        Bson filter = Filters.eq(keyName,obj.getIdFilm());
        Bson update = Updates.combine(
                Updates.set("year",obj.getYear()),
                         Updates.set("name",obj.getName())
        );
        database.getCollection(tableName).updateOne(filter,update);
    }


    public void delete(Film obj) throws MongoException {

        if(obj == null || obj.getIdFilm() == null){
            return;
        }

        Bson filter = Filters.and(
                Filters.eq(keyName, obj.getIdFilm()),
                Filters.eq("name", obj.getName()),
                Filters.eq("year", obj.getYear())
        );
        database.getCollection(tableName).deleteOne(filter);
    }


    public void deleteById(Integer id) throws MongoException {

        Bson filter = Filters.eq(keyName, id);
        database.getCollection(tableName).deleteOne(filter);
    }

    public void deleteAll(){
        database.getCollection(tableName).deleteMany(new Document());
    }

    public Film findById(Integer id) throws MongoException {

        Bson filter = Filters.eq(keyName, id);
        FindIterable<Document> iterable = database.getCollection(tableName).find(filter);

        if(iterable.iterator().hasNext()){
            Document document =  iterable.iterator().next();
            return new Gson().fromJson(document.toJson(), Film.class);
        } else{
            return null;
        }
    }

    public Film findByNameAndYear(String name,int year) throws MongoException {

        Bson filter =   Filters.and(
                 Filters.eq("name", name),
                        Filters.eq("year",year)
        );

        FindIterable<Document> iterable = database.getCollection(tableName).find(filter);

        if(iterable.iterator().hasNext()){
            Document document =  iterable.iterator().next();
            return new Gson().fromJson(document.toJson(), Film.class);
        } else{
            return null;
        }
    }


    public Film[] findAll() throws MongoException {

        Gson gson = new Gson();
        List<Film> filmlist = new LinkedList<>();
        FindIterable<Document> iterable = database.getCollection(tableName).find();
        Iterator<Document> iterator = iterable.iterator();

        if(iterator.hasNext()){
            while (iterator.hasNext()) {
                Document document = iterator.next();
                Film film = gson.fromJson(document.toJson(), Film.class);
                filmlist.add(film);
            }
            return filmlist.toArray(Film[]::new);
        } else {
            return null;
        }
    }

    public LinkedList<Integer> findRelatedActor(Integer idFilm) throws MongoException {

        if(this.findById(idFilm)!=null){

            Film film = this.findById(idFilm);
            LinkedList<Integer> filmActors = film.getIdActeur();
            return filmActors;

        }
        return null;
    }

}
