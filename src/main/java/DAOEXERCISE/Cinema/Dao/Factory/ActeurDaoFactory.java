package DAOEXERCISE.Cinema.Dao.Factory;

import DAOEXERCISE.Cinema.Interface.IActeurDao;
import DAOEXERCISE.Cinema.Dao.Mongodb.ActeurDaoMongodb;
import DAOEXERCISE.Cinema.Dao.Mysql.ActeurDaoMysql;
import com.mongodb.client.MongoClient;
import java.sql.Connection;

public class ActeurDaoFactory {

    static IActeurDao acteurDao = null;

    public static IActeurDao getDao(Connection sqlConnection) {

        if(acteurDao == null) {
            return acteurDao = new ActeurDaoMysql(sqlConnection);
        }
        return acteurDao;
    }

    public static IActeurDao getDao(MongoClient mongoConnection) {

        if(acteurDao == null) {
            return new ActeurDaoMongodb(mongoConnection);
        }
        return acteurDao;

    }






}
