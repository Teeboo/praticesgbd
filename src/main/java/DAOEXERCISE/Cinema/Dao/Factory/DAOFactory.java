package DAOEXERCISE.Cinema.Dao.Factory;

import DAOEXERCISE.Cinema.Dao.ActeurDAO;
import DAOEXERCISE.Cinema.Dao.FilmDAO;
import DAOEXERCISE.Cinema.config.ConfigReader;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOFactory {


    // VARIABLES

    private static String db;
    static ConfigReader configReader = new ConfigReader();
    public static final String dbName = "cinema";
    public static  String username = null;
    public static  String password = null;
    private static DAOFactory daoFactory = null;

    private ActeurDAO acteurDAO;
    private FilmDAO filmDAO;

    private MongoClient mongodbConnection = null;
    private Connection mysqlConnection = null;
    private final String mongodbUrl = "mongodb://localhost:27017";

    private final String mysqlDriver = "com.mysql.cj.jdbc.Driver";
    private final String mysqlUrl = "jdbc:mysql://localhost:3306/" + dbName + "?serverTimezone=UTC";


    // CONSTRUCTOR

    private DAOFactory() {

        if(db.equals("Mongodb")) {

            mongodbConnection = getMongoDbConnection();
            mongodbConnection.startSession();

        } else if (db.equals("Mysql")) {

            username = "root";
            password = "";
            mysqlConnection = getMysqlConnection();
        }
    }

    public static DAOFactory connectTo() {

        if (daoFactory == null) {
            db = configReader.getDatabaseType();
            daoFactory = new DAOFactory();
        }
        return daoFactory;
    }


    // GETTER

    public ActeurDAO getActeurDAO(){

        if(acteurDAO == null){

            if(db.equals("Mongodb")){
                acteurDAO = new ActeurDAO(mongodbConnection);
                return acteurDAO;
            } else if (db.equals("Mysql")) {
                acteurDAO = new ActeurDAO(mysqlConnection);
                return acteurDAO;
            }

        }
        return acteurDAO;
    }

    public FilmDAO getFilmDAO(){

        if(filmDAO == null){

            if(db.equals("Mongodb")){
                filmDAO = new FilmDAO(mongodbConnection);
                return filmDAO;
            } else if (db.equals("Mysql")) {
                filmDAO  = new FilmDAO(mysqlConnection);
                return filmDAO;
            }

        }
        return filmDAO;
    }

    // FUNCTION

    private MongoClient getMongoDbConnection() {

        if (mongodbConnection == null) {
            mongodbConnection = MongoClients.create(mongodbUrl);
        }
        return mongodbConnection;
    }

    private Connection getMysqlConnection() {

        if (mysqlConnection == null) {
            try {
                Class.forName(mysqlDriver);
                mysqlConnection = DriverManager.getConnection(mysqlUrl, username, password);
                mysqlConnection.setAutoCommit(true);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(DAOFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return mysqlConnection;

    }




}
