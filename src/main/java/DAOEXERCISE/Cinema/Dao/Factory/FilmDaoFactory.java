package DAOEXERCISE.Cinema.Dao.Factory;

import DAOEXERCISE.Cinema.Dao.Mongodb.FilmDaoMongodb;
import DAOEXERCISE.Cinema.Dao.Mysql.FilmDaoMysql;
import DAOEXERCISE.Cinema.Interface.IFilmDao;
import com.mongodb.client.MongoClient;

import java.sql.Connection;

public class FilmDaoFactory {

    static IFilmDao iFilmDao = null;

    public static IFilmDao getDao(Connection sqlConnection) {

        if(iFilmDao == null) {
            return iFilmDao = new  FilmDaoMysql(sqlConnection);
        }
        return iFilmDao;
    }

    public static IFilmDao getDao(MongoClient mongoConnection) {

        if(iFilmDao == null) {
            return new FilmDaoMongodb(mongoConnection);
        }
        return iFilmDao;

    }
}
