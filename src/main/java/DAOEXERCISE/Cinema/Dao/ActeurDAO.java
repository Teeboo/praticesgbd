package DAOEXERCISE.Cinema.Dao;

import DAOEXERCISE.Cinema.Dao.Factory.ActeurDaoFactory;
import DAOEXERCISE.Cinema.Interface.DAO;
import DAOEXERCISE.Cinema.Interface.IActeurDao;
import DAOEXERCISE.Cinema.beans.Acteur;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

public class ActeurDAO extends DAO<Acteur,Integer> {


    // VARIABLE

    private final IActeurDao acteurDao;


    // CONSTRUCTOR


    public ActeurDAO(MongoClient connection){

        this.acteurDao = ActeurDaoFactory.getDao(connection);
    }

    public ActeurDAO(Connection connection){

        this.acteurDao = ActeurDaoFactory.getDao(connection);

    }


    // FUNCTION


    @Override
    public void create(Acteur obj) throws SQLException,MongoException {

        acteurDao.create(obj);
    }

    @Override
    public void updateFromId(Acteur obj) throws SQLException,MongoException{

        acteurDao.updateFromId(obj);

    }

    @Override
    public void delete(Acteur obj)throws SQLException,MongoException{

        acteurDao.delete(obj);
    }

    @Override
    public void deleteById(Integer id)throws SQLException,MongoException{

        acteurDao.deleteById(id);
    }

    public void deleteAll()throws SQLException,MongoException{

        acteurDao.deleteAll();
    }

    @Override
    public Acteur findById(Integer id)throws SQLException,MongoException{

        return acteurDao.findById(id);
    }

    public Integer findIdByFullName(String lastname, String firstname )throws SQLException,MongoException{

        return acteurDao.findIdByFullName(lastname,firstname);

    }

    @Override
    public Acteur[] findAll() throws SQLException, MongoException {

        return acteurDao.findAll();
    }

    public LinkedList<Integer> findRelatedMovie(Integer id) throws SQLException, MongoException {

        return acteurDao.findRelatedMovie(id);

    }








}
